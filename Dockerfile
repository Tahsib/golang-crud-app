FROM golang:1.16-alpine

WORKDIR /app

COPY go.mod ./
COPY go.sum ./
RUN go mod download

COPY *.go ./
COPY form/* ./form/

RUN go build -o /goapp

EXPOSE 8080

CMD [ "/goapp" ]